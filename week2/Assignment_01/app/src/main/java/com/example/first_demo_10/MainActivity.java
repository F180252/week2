package com.example.first_demo_10;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button logbtn;
    EditText userName;
    EditText userPassword;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img = findViewById((R.id.img));
        userName= findViewById(R.id.userName);
        userPassword= findViewById(R.id.userPassword);
        logbtn= findViewById(R.id.logbtn);
        logbtn.setOnClickListener(this);
        img.setImageResource(R.drawable.login);
    }

    @Override
    public void onClick(View view) {
        String email = userName.getText().toString();
        String pass = userPassword.getText().toString();
        if(email.equals("18F0252") && pass.equals("12345678"))
        {
            Toast.makeText(MainActivity.this,"Correct mail or pass",Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(MainActivity.this,"Wrong mail or pass",Toast.LENGTH_SHORT).show();
        }
    }
};